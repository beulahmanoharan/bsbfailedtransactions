package BSB.BSB;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class bsbFailedTransactions {

	
	boolean flag = false;
	int j, rowStart = 1, rowEnd;

	List<WebElement> rows, failureRows, innerTableRows;
	List<WebElement> columns, failureColumns, innerTableColumns;
	List<String> transactionId = new ArrayList<String>();
	List<String> inputData = new ArrayList<String>();

	WebElement loadingText;
	WebElement table, innerTable;
	WebElement tableBody, innerTableBody;
	WebElement failureHistoryTable;
	WebElement failureTableBody;
	WebElement techName, partNumber, vanId;

	String interfaceId, dataCategory, frmDate, toDate, sortWithTransactionStatus, iterate;
	String errorMessage = null, errorMsg;
	int noOfIterations = 0, noOfRecords = 0, pagination = 0;
	public String paginationText;
	String url = "http://lv-irisapp01:8080/BallyServiceBus/UI/Home";
			
	Screen s = new Screen();
		
	int a = 0, b = 0, d = 1;
	

	//ChromeOptions opt = new ChromeOptions();
	public ChromeDriver driver;
	
	public XSSFWorkbook bsbDataFile;
	public XSSFWorkbook transDetailsWorkbook;

	@Test
	public void primaryMethod() throws FindFailed, IOException, InterruptedException, AWTException {
		System.setProperty("webdriver.chrome.driver", "./drivers/ChromeDriver.exe");

		//opt.setBinary("C:\\Users\\bmanoharan\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
		driver = new ChromeDriver();

		WebDriverWait wait = new WebDriverWait(driver, 50);

		Actions action = new Actions(driver);

		driver.manage().window().maximize();
		driver.get(url);
		driver.manage().timeouts().pageLoadTimeout(90, TimeUnit.SECONDS);

		// Create an object of File class to open xlsx file
		File file = new File("E:\\Selenium\\BSBInputData.xlsx");

		// Create an object of FileInputStream class to read excel file
		FileInputStream inputStream = new FileInputStream(file);

		// Read data from input worbook
		bsbDataFile = new XSSFWorkbook(inputStream);

		// Read sheet inside the workbook by its name
		XSSFSheet bsbInput = bsbDataFile.getSheet("Sheet1");

		// Create an object of FileOutputStream class to create write data in
		// excel file
		FileOutputStream outputStream = new FileOutputStream(new File("E:\\Selenium\\BSBFailureHistory.xlsx"));

		// Blank workbook
		transDetailsWorkbook = new XSSFWorkbook();

		// Create a blank sheet
		XSSFSheet transDetailSheet = transDetailsWorkbook.createSheet("TransDetails");

		// Create a loop over all the rows of excel file to read it
		XSSFRow row = bsbInput.getRow(1);

		// Find number of rows in excel file
		System.out.println("Last Row Number..." + bsbInput.getLastRowNum());
		System.out.println("First Row Number..." + bsbInput.getFirstRowNum());
		int rowCount = bsbInput.getLastRowNum() - bsbInput.getFirstRowNum();
		System.out.println(rowCount);

		frmDate = row.getCell(0).getStringCellValue();
		toDate = row.getCell(1).getStringCellValue();
		interfaceId = row.getCell(2).getStringCellValue();
		dataCategory = row.getCell(3).getStringCellValue();
		sortWithTransactionStatus = row.getCell(4).getStringCellValue();
		iterate = row.getCell(5).getStringCellValue();
		noOfRecords = (int) row.getCell(6).getNumericCellValue();

		System.out.print("Excel Data..." + frmDate + "|| " + toDate + "|| " + interfaceId + "|| " + dataCategory + "||"
				+ sortWithTransactionStatus + "|| " + iterate + "...");

		s.find("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\TransactionManager.png");
		s.mouseMove("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\TransactionManager.png");

		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		s.mouseMove("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\TransactionManager.png");
		s.click();

		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		// wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//span[text()[contains(.,'Search')]])[2]"))));
		// System.out.println("Search button
		// location..."+driver.findElement(By.xpath("(//span[text()[contains(.,'Search')]])[2]")).getLocation());

		System.out.println("Location of start of the page..."
				+ driver.findElement(By.xpath("//div[@id='transaction_welcome']")).getLocation());
		System.out.println("Location of the title Transaction Manager..."
				+ driver.findElement(By.xpath("//div[contains(text(),'Transaction Manager')]")).getLocation());
		System.out.println("Location of the title Available Interfaces..."
				+ driver.findElement(By.xpath("//div[contains(text(),'Available Interfaces')]")).getLocation());
		System.out.println("Location of the Anchor tag of search button...."
				+ driver.findElement(By.xpath("//a[@class='trigger']")).getLocation());

		s.wait("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\Search.png");
		s.find("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\Search.png");
		s.mouseMove("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\Search.png");
		s.mouseMove("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\Search.png");
		s.click();

		driver.manage().timeouts().pageLoadTimeout(90, TimeUnit.SECONDS);

		WebElement fromDate = driver.findElement(By.id("dateFrom"));
		wait.until(ExpectedConditions.visibilityOf(fromDate));
		fromDate.click();
		fromDate.sendKeys(frmDate,Keys.ENTER);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);

		WebElement datePicker = driver.findElement(By.className("ui-datepicker-title"));
		WebElement toDateValue = driver.findElement(By.id("dateTo"));
		wait.until(ExpectedConditions.invisibilityOf(datePicker));
		toDateValue.click();
		toDateValue.sendKeys(toDate, Keys.ENTER);

		WebElement interfaceIdValue = driver.findElement(By.id("searchBean.interfaceID"));
		wait.until(ExpectedConditions.invisibilityOf(datePicker));
		Select chooseInterface = new Select(interfaceIdValue);
		chooseInterface.selectByVisibleText(interfaceId);

		WebElement dataCategoryId = driver.findElement(By.id("searchBean.dataCategoryID"));
		Select chooseDataCategory = new Select(dataCategoryId);
		chooseDataCategory.selectByVisibleText(dataCategory);

		System.out.println("Location of search button..." + driver.findElement(By.id("bttnSearch")).getLocation());

		s.find("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\SearchButton3.png");
		s.mouseMove("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\SearchButton3.png");
		s.mouseMove("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\SearchButton3.png");
		s.click();

		loadingText = driver.findElement(By.xpath("//div[contains(text(),'Loading')]"));
		if (loadingText.isDisplayed())
			flag = true;
		System.out.println("Flag..." + flag);

		wait.until(ExpectedConditions.invisibilityOf(loadingText));

		table = driver.findElement(By.id("list47"));
		wait.until(ExpectedConditions.visibilityOf(table));

		// First row Id of the table which means the grid content has been populated.
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//tr[@class='ui-widget-content jqgrow ui-row-ltr']"))));

		paginationText = driver.findElement(By.className("ui-paging-info")).getText();
		System.out.println("Length of the Pagination text content..." + paginationText.length());
		String records = (paginationText.substring(paginationText.length() - 2));
		System.out.println("Records..." + records);
		records.trim().trim();
		System.out.println("Records..." + records);

		System.out.println("Total No of Records..." + noOfRecords);

		if (sortWithTransactionStatus.contains("Yes")) 
		{
			driver.findElement(By.id("jqgh_list47_TRANSACTION_STATUS")).click();

			// First row Id of the table which means the grid content has been populated.
			wait.until(ExpectedConditions
					.visibilityOf(driver.findElement(By.xpath("//tr[@class='ui-widget-content jqgrow ui-row-ltr']"))));
		}

		loadingText = driver.findElement(By.xpath("//div[contains(text(),'Loading')]"));
		if (loadingText.isDisplayed())
			flag = true;
		System.out.println("Flag..." + flag);
		wait.until(ExpectedConditions.invisibilityOf(loadingText));
		wait.until(ExpectedConditions.visibilityOf(table));

		// Method Call for a set of transactions.
		noOfIterations = Integer.parseInt(iterate);
		System.out.println("Number of times to iterate..."+noOfIterations);

		b = trackErrorMessage(driver, wait, transDetailSheet, sortWithTransactionStatus, table, row, rowStart, a,
				noOfIterations);
		System.out.println("Returned Value of recursive function..."+b);

		// Write to excel.
		transDetailsWorkbook.write(outputStream);
		outputStream.close();

		System.out.println("End of the program...");

	}

	/*@AfterTest
	public void close() throws IOException
	{		
		driver.close();
		bsbDataFile.close();
		transDetailsWorkbook.close();
	}*/
	public int trackErrorMessage(WebDriver driver, WebDriverWait wait, XSSFSheet transDetailSheet,
			String sortWithTransactionStatus2, WebElement table, XSSFRow row, int rowStart, int times, int n)
			throws FindFailed, AWTException, InterruptedException {

		System.out.println("RowStart Value when function begins..." + rowStart);

		table = driver.findElement(By.id("list47"));

		System.out.println("Value of a when execution of the function begins..." + a);

		if (times > 0) 
		{
			System.out.println("Scroll down function is called....");
			nextIteration(driver, wait, table, times, rowStart, noOfRecords);
		}

		System.out.println("After scrolling down...");

		loadingText = driver.findElement(By.id("load_list47"));
		if (loadingText.isDisplayed())
			flag = true;

		System.out.println("Flag..." + flag);
		wait.until(ExpectedConditions.invisibilityOf(loadingText));

		table = driver.findElement(By.id("list47"));
		wait.until(ExpectedConditions.visibilityOf(table));

		tableBody = table.findElement(By.tagName("tbody"));
		rows = tableBody.findElements(By.tagName("tr"));

		columns = rows.get(1).findElements(By.tagName("td"));
		System.out.println("No of Columns..." + columns.size());

		int rcount = rows.size();
		System.out.println("Number of rows...." + rcount);

		System.out.println("RowStart Value before the for loop starts..." + rowStart);

		for (int i = rowStart; i < rcount; i++) {
			columns = rows.get(i).findElements(By.tagName("td"));

			if ((columns.get(2).getText().contains("Failed-Error"))
					|| (columns.get(2).getText().contains("Failed-System Unavailable"))) {
				transactionId.add(columns.get(1).getText());
				System.out.println("Transaction Ids..." + transactionId);
				
				// To enable the checkbox
				// columns.get(0).click();
			}

			System.out.println("rCount value..." + rcount);
		}

		System.out.println("Transaction Ids..." + transactionId);

		for (j = rowStart; j < rcount; j++) {
			columns = rows.get(j).findElements(By.tagName("td"));
			System.out.println("No of Columns..." + columns.size());

			// Create a new row and append it at last of sheet
			Row newRow = transDetailSheet.getRow(j);
			if (newRow == null)
				row = transDetailSheet.createRow(j);

			int k = 0;// variable to increase cell count of the row

			// Fill data in row - Set Transaction ID
			transDetailSheet.getRow(j).createCell(k).setCellValue(columns.get(1).getText());
			// Cell cell = newRow.getCell(k);
			// cell.setCellValue(columns.get(1).getText());

			// Get the current count of rows in excel file
			int rowCount1 = transDetailSheet.getLastRowNum() - transDetailSheet.getFirstRowNum();
			System.out.println("Count of rows in excel file currently..." + rowCount1);

			if ((columns.get(2).getText().contains("Failed-Error"))
					|| (columns.get(2).getText().contains("Failed-System Unavailable"))) {
				wait.until(ExpectedConditions.elementToBeClickable(columns.get(1)));
				columns.get(1).click();
				driver.manage().timeouts().pageLoadTimeout(90, TimeUnit.SECONDS);
				failureHistoryTable = driver.findElement(By.id("treeFailureHistory"));
				failureTableBody = failureHistoryTable.findElement(By.tagName("tbody"));
				failureRows = failureTableBody.findElements(By.tagName("tr"));
				failureColumns = failureRows.get(0).findElements(By.tagName("td"));

				innerTable = driver.findElement(By.id("treeRepost"));
				innerTableBody = innerTable.findElement(By.tagName("tbody"));
				innerTableRows = innerTableBody.findElements(By.tagName("tr"));
				innerTableColumns = innerTableRows.get(1).findElements(By.tagName("td"));

				// Set Header Row of Output Excel File...
				Row headerRow = transDetailSheet.getRow(0);
				if (headerRow == null)
					row = transDetailSheet.createRow(0);

				transDetailSheet.getRow(0).createCell(0).setCellValue("Trans ID");
				transDetailSheet.getRow(0).createCell(1).setCellValue("Error Desc");

				if (dataCategory.equalsIgnoreCase("GameServicePMPartsUsedTrans")) {
					transDetailSheet.getRow(0).createCell(2).setCellValue("Part Number");
					transDetailSheet.getRow(0).createCell(3).setCellValue("Van Id");
					transDetailSheet.getRow(0).createCell(4).setCellValue("Tech Name");
				}
				if (dataCategory.equalsIgnoreCase("InventoryTransactions")) {
					transDetailSheet.getRow(0).createCell(2).setCellValue("Part Number");
					transDetailSheet.getRow(0).createCell(3).setCellValue("Van Id");
					transDetailSheet.getRow(0).createCell(4).setCellValue("Tech Name");
				}
				if (dataCategory.equalsIgnoreCase("AttachmentTransaction")) {
					transDetailSheet.getRow(0).createCell(2).setCellValue("SR Number");
					transDetailSheet.getRow(0).createCell(3).setCellValue("AssetNumber");
				}

				if (dataCategory.equalsIgnoreCase("BQIRTransaction")) {
					transDetailSheet.getRow(0).createCell(2).setCellValue("SR Number");
				}
				if (dataCategory.equalsIgnoreCase("CreateSRTransaction")) {
					transDetailSheet.getRow(0).createCell(2).setCellValue("Work Order Number");
					transDetailSheet.getRow(0).createCell(3).setCellValue("Manifest Id");
				}

				if (dataCategory.equalsIgnoreCase("DeliveryAndAcceptance")) {
					transDetailSheet.getRow(0).createCell(2).setCellValue("SR Number");
					transDetailSheet.getRow(0).createCell(3).setCellValue("AsteaServiceOrderId");
				}

				if (dataCategory.equalsIgnoreCase("GoLiveTransaction")) {
					transDetailSheet.getRow(0).createCell(2).setCellValue("SR Number");
					transDetailSheet.getRow(0).createCell(3).setCellValue("AsteaServiceOrderId");
				}

				if (dataCategory.equalsIgnoreCase("InstalledTransaction")) {
					transDetailSheet.getRow(0).createCell(2).setCellValue("SR Number");
					transDetailSheet.getRow(0).createCell(3).setCellValue("AsteaServiceOrderId");
				}

				for (WebElement transactionDetails : failureRows) {
					failureColumns = transactionDetails.findElements(By.tagName("td"));
					System.out.println("Transaction Details..." + transactionDetails.getText());

					errorMsg = failureColumns.get(2).getText();

					if (errorMessage == null)
						errorMessage = errorMsg + "\n" + "\n";
					else
						errorMessage = errorMessage + errorMsg + "\n" + "\n";

					System.out.println("Error Message..." + errorMessage);
					/*
					 * if(dataCategory.equalsIgnoreCase("DeliveryAndAcceptance")
					 * ) { if(errorMessage.contains("SocketTimeoutException")) {
					 * 
					 * s.find(
					 * "C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\Repost.png"
					 * ); s.mouseMove(
					 * "C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\Repost.png"
					 * ); s.click();
					 * 
					 * loadingText = driver.findElement(By.xpath(
					 * "//div[contains(text(),'Loading')]"));
					 * if(loadingText.isDisplayed()) flag=true;
					 * 
					 * System.out.println("Flag..."+flag);
					 * 
					 * wait.until(ExpectedConditions.invisibilityOf(loadingText)
					 * );
					 * wait.until(ExpectedConditions.visibilityOfElementLocated(
					 * By.xpath("//div[@class='success']")));
					 * 
					 * innerTable = driver.findElement(By.id("treeRepost"));
					 * wait.until(ExpectedConditions.visibilityOf(innerTable));
					 * 
					 * } }
					 */

					/*
					 * if(dataCategory.equalsIgnoreCase(
					 * "GameServicePMPartsUsedTrans")) {
					 * if(errorMessage.contains("not valid")) { //for(int
					 * x=1;x<innerTableRows.size();x++) //{
					 * //if(innerTableRows.get(x).findElement(By.xpath(
					 * "//td/label/text()[contains(.,'TechName')]"))!=null) //{
					 * techName = driver.findElement(By.id("value13")); String
					 * incorrectTechName = techName.getAttribute("value");
					 * 
					 * partNumber = driver.findElement(By.id("value7"));
					 * System.out.println("Part Number...."+partNumber.
					 * getAttribute("value"));
					 * 
					 * System.out.println("Incorrect Tech Name..."
					 * +incorrectTechName);
					 * 
					 * if(incorrectTechName.contains("105861")) {
					 * techName.clear(); techName.sendKeys("JFUENTES"); }
					 * 
					 * s.find(
					 * "C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\Repost.png"
					 * ); s.mouseMove(
					 * "C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\Repost.png"
					 * ); s.click();
					 * 
					 * loadingText = driver.findElement(By.xpath(
					 * "//div[contains(text(),'Loading')]"));
					 * if(loadingText.isDisplayed()) flag=true;
					 * 
					 * System.out.println("Flag..."+flag);
					 * 
					 * wait.until(ExpectedConditions.invisibilityOf(loadingText)
					 * );
					 * wait.until(ExpectedConditions.visibilityOfElementLocated(
					 * By.xpath("//div[@class='success']")));
					 * 
					 * innerTable = driver.findElement(By.id("treeRepost"));
					 * wait.until(ExpectedConditions.visibilityOf(innerTable));
					 * 
					 * 
					 * 
					 * //} //} } }
					 */

					/*
					 * if(dataCategory.equalsIgnoreCase("InventoryTransactions")
					 * ) { if(errorMessage.contains("not valid")) { techName =
					 * driver.findElement(By.id("value5")); String
					 * incorrectTechName = techName.getAttribute("value");
					 * 
					 * partNumber = driver.findElement(By.id("value1"));
					 * System.out.println("Part Number...."+partNumber.
					 * getAttribute("value"));
					 * 
					 * vanId = driver.findElement(By.id("value7"));
					 * System.out.println("Part Number...."+vanId.getAttribute(
					 * "value"));
					 * 
					 * System.out.println("Incorrect Tech Name..."
					 * +incorrectTechName);
					 * 
					 * if(incorrectTechName.contains("STEVEN.ANGLE")) {
					 * techName.clear(); techName.sendKeys("SANGLE"); }
					 * 
					 * if(incorrectTechName.contains("MARK.WORTHEN")) {
					 * techName.clear(); techName.sendKeys("MARK.WORTHEN"); }
					 * s.find(
					 * "C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\Repost.png"
					 * ); s.mouseMove(
					 * "C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\Repost.png"
					 * ); s.click();
					 * 
					 * loadingText = driver.findElement(By.xpath(
					 * "//div[contains(text(),'Loading')]"));
					 * if(loadingText.isDisplayed()) flag=true;
					 * 
					 * System.out.println("Flag..."+flag);
					 * 
					 * wait.until(ExpectedConditions.invisibilityOf(loadingText)
					 * );
					 * wait.until(ExpectedConditions.visibilityOfElementLocated(
					 * By.xpath("//div[@class='success']")));
					 * 
					 * innerTable = driver.findElement(By.id("treeRepost"));
					 * wait.until(ExpectedConditions.visibilityOf(innerTable));
					 * } }
					 * 
					 */

				}

				k++;
				// Fill data in row - Set Transaction's Error Description
				transDetailSheet.getRow(j).createCell(k).setCellValue(errorMessage);
				System.out.println("Value of k..." + k);
			
				if (dataCategory.equalsIgnoreCase("GameServicePMPartsUsedTrans")) 
				{

					if (errorMessage.contains("Company No")) 
					{
						System.out.println("There are no details like part number, van id... for this transaction...");
					} 
					
					else /*if ((errorMessage.contains("From Location"))
							||(errorMessage.contains("Location Quantity"))||(errorMessage.contains("Mapics user"))
							||((errorMessage.contains("Mapics user")) && (errorMessage.contains("Location Quantity")))) */
					{
						k++;
						String partNo =driver.findElement(By.xpath("//label[contains(.,'PartNumber')]/following::input")).getAttribute("value");
						transDetailSheet.getRow(j).createCell(k).setCellValue(partNo);

						k++;
						String vanId =driver.findElement(By.xpath("//label[contains(.,'VanId')]/following::input")).getAttribute("value");						
						transDetailSheet.getRow(j).createCell(k).setCellValue(vanId);
					
						k++;
						String techNam = driver.findElement(By.xpath("//label[contains(.,'TechName')]/following::input")).getAttribute("value");	
						transDetailSheet.getRow(j).createCell(k).setCellValue(techNam);
					
					}

			
				}
				if (dataCategory.equalsIgnoreCase("InventoryTransactions")) 
				{
					/*if (errorMessage.contains("Location Quantity")||(errorMessage.contains("not valid Mapics user"))
							||(errorMessage.contains("not valid"))||(errorMessage.contains("ISL Order"))
							||(errorMessage.contains("No Company"))) {*/
						k++;
						String partNo =driver.findElement(By.xpath("//label[contains(.,'PartNumber')]/following::input")).getAttribute("value");
						transDetailSheet.getRow(j).createCell(k).setCellValue(partNo);

						k++;
						String vanId =driver.findElement(By.xpath("//label[contains(.,'VanId')]/following::input")).getAttribute("value");						
						transDetailSheet.getRow(j).createCell(k).setCellValue(vanId);
					
						k++;
						String techNam = driver.findElement(By.xpath("//label[contains(.,'TechName')]/following::input")).getAttribute("value");	
						transDetailSheet.getRow(j).createCell(k).setCellValue(techNam);
					//} 
					
				}

				if (dataCategory.equalsIgnoreCase("AttachmentTransaction")) {
					k++;
					String srNumber = driver.findElement(By.id("repostBean.xmlContent[3].value")).getAttribute("value");
					transDetailSheet.getRow(j).createCell(k).setCellValue(srNumber);

					k++;
					String asteaServiceOrderId = driver.findElement(By.id("repostBean.xmlContent[57].value"))
							.getAttribute("value");
					transDetailSheet.getRow(j).createCell(k).setCellValue(asteaServiceOrderId);
				}

				if (dataCategory.equalsIgnoreCase("BQIRTransaction")) {
					k++;
					String srNumber = driver.findElement(By.id("repostBean.xmlContent[41].value"))
							.getAttribute("value");
					transDetailSheet.getRow(j).createCell(k).setCellValue(srNumber);
				}

				if (dataCategory.equalsIgnoreCase("CreateSRTransaction")) {
					k++;
					String workOrderNumber = driver.findElement(By.id("repostBean.xmlContent[32].value"))
							.getAttribute("value");
					transDetailSheet.getRow(j).createCell(k).setCellValue(workOrderNumber);

					k++;
					String manifestId = driver.findElement(By.id("repostBean.xmlContent[1].value"))
							.getAttribute("value");
					transDetailSheet.getRow(j).createCell(k).setCellValue(manifestId);
				}

				if (dataCategory.equalsIgnoreCase("DeliveryAndAcceptance")) {
					k++;
					String srNumber = driver.findElement(By.id("repostBean.xmlContent[4].value")).getAttribute("value");
					transDetailSheet.getRow(j).createCell(k).setCellValue(srNumber);

					k++;
					String asteaServiceOrderId = driver.findElement(By.id("repostBean.xmlContent[55].value"))
							.getAttribute("value");
					transDetailSheet.getRow(j).createCell(k).setCellValue(asteaServiceOrderId);
				}

				if (dataCategory.equalsIgnoreCase("GoLiveTransaction")) {
					k++;
					String srNumber = driver.findElement(By.id("repostBean.xmlContent[4].value")).getAttribute("value");
					transDetailSheet.getRow(j).createCell(k).setCellValue(srNumber);

					k++;
					String asteaServiceOrderId = driver.findElement(By.id("repostBean.xmlContent[53].value"))
							.getAttribute("value");
					transDetailSheet.getRow(j).createCell(k).setCellValue(asteaServiceOrderId);
				}

				if (dataCategory.equalsIgnoreCase("InstalledTransaction")) {
					k++;
					String srNumber = driver.findElement(By.id("repostBean.xmlContent[4].value")).getAttribute("value");
					transDetailSheet.getRow(j).createCell(k).setCellValue(srNumber);

					k++;
					String asteaServiceOrderId = driver.findElement(By.id("repostBean.xmlContent[55].value"))
							.getAttribute("value");
					transDetailSheet.getRow(j).createCell(k).setCellValue(asteaServiceOrderId);
				}

				errorMessage = null;
				errorMsg = null;

				s.find("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\Back.png");
				s.mouseMove("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\Back.png");
				s.click();

				driver.manage().timeouts().pageLoadTimeout(90, TimeUnit.SECONDS);

				loadingText = driver.findElement(By.xpath("//div[contains(text(),'Loading')]"));
				if (loadingText.isDisplayed())
					flag = true;

				System.out.println("Flag..." + flag);

				wait.until(ExpectedConditions.invisibilityOf(loadingText));

				table = driver.findElement(By.id("list47"));
				wait.until(ExpectedConditions.visibilityOf(table));

				// First row Id of the table which means the grid content has
				// been populated.
				wait.until(ExpectedConditions.visibilityOf(
						driver.findElement(By.xpath("//tr[@class='ui-widget-content jqgrow ui-row-ltr']"))));

				if (sortWithTransactionStatus.contains("Yes")) {
					driver.findElement(By.id("jqgh_list47_TRANSACTION_STATUS")).click();
					// First row Id of the table which means the grid content
					// has been populated.
					wait.until(ExpectedConditions.visibilityOf(
							driver.findElement(By.xpath("//tr[@class='ui-widget-content jqgrow ui-row-ltr']"))));
				}

				loadingText = driver.findElement(By.xpath("//div[contains(text(),'Loading')]"));
				if (loadingText.isDisplayed())
					flag = true;

				System.out.println("Flag..." + flag);
				wait.until(ExpectedConditions.invisibilityOf(loadingText));
				wait.until(ExpectedConditions.visibilityOf(table));

				table = driver.findElement(By.id("list47"));
				tableBody = table.findElement(By.tagName("tbody"));
				rows = tableBody.findElements(By.tagName("tr"));

				if (times > 0) {
					// Method Call
					System.out.println("Scroll down function is called....");
					nextIteration(driver, wait, table, times, rowStart, noOfRecords);

					System.out.println("After scrolling down...");

					s.find("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\scrolldownarrow.png");
					s.mouseMove("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\scrolldownarrow.png");
					s.mouseMove("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\scrolldownarrow.png");

					s.click();

					loadingText = driver.findElement(By.id("load_list47"));
					if (loadingText.isDisplayed())
						flag = true;

					System.out.println("Flag..." + flag);
					wait.until(ExpectedConditions.invisibilityOf(loadingText));

					table = driver.findElement(By.id("list47"));
					wait.until(ExpectedConditions.visibilityOf(table));

					tableBody = table.findElement(By.tagName("tbody"));
					rows = tableBody.findElements(By.tagName("tr"));
					columns = rows.get(1).findElements(By.tagName("td"));

					System.out.println("No of Columns..." + columns.size());
					System.out.println("Number of rows...." + rows.size());

				}

				loadingText = driver.findElement(By.id("load_list47"));
				if (loadingText.isDisplayed())
					flag = true;

				System.out.println("Flag..." + flag);
				wait.until(ExpectedConditions.invisibilityOf(loadingText));
			}
		}
		a = a + 1;

		rows.clear();
		columns.clear();
		System.out.println("Value of a when execution of the function ends..." + a);

		if (a <= n) {
			System.out.println("Value of a in recursive call of the function..." + a);
			return trackErrorMessage(driver, wait, transDetailSheet, sortWithTransactionStatus, table, row,
					rowStart + 10, a, noOfIterations);
		} else
			return 0;
	}

	public void nextIteration(WebDriver driver, WebDriverWait wait, WebElement table, int noOfTimes, int rowStartNo,
			int totalCountOfRecords) throws FindFailed, AWTException {
		for (int q = 1; q <= noOfTimes; q++) {
			System.out.println();
			System.out.println("No of times....." + q);

			s.find("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\scrolldownarrow.png");
			s.mouseMove("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\scrolldownarrow.png");
			s.mouseMove("C:\\Users\\bmanoharan\\workspace\\BSB\\Images\\scrolldownarrow.png");

			s.click();
			s.click();

			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_PAGE_DOWN);
			robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
			System.out.println("Page down....");

			loadingText = driver.findElement(By.id("load_list47"));
			if (loadingText.isDisplayed())
				flag = true;

			System.out.println("Flag..." + flag);
			wait.until(ExpectedConditions.invisibilityOf(loadingText));

			table = driver.findElement(By.id("list47"));
			wait.until(ExpectedConditions.visibilityOf(table));

		}

		System.out.println("RowStart Value..." + rowStartNo);
		pagination = (rowStartNo + 9);

		if (pagination > totalCountOfRecords)
			pagination = totalCountOfRecords;

		paginationText = Integer.toString(pagination);
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("ui-paging-info"), paginationText));
		System.out.println("Total record count..." + driver.findElement(By.className("ui-paging-info")).getText());
		System.out.println("Pagination...." + pagination);

		loadingText = driver.findElement(By.id("load_list47"));
		if (loadingText.isDisplayed())
			flag = true;

		System.out.println("Flag..." + flag);
		wait.until(ExpectedConditions.invisibilityOf(loadingText));

	}
}
